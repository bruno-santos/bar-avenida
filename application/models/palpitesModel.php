<?php
class palpitesModel extends CI_Model {

    public function select() {
        return $this->db->get('palpites')->result();
    }

    public function insert($dados) {
        return $this->db->insert('palpites', $dados);
    } 

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('palpites');
    }

    public function find($id) {
        $this->db->where('id', $id);
        return $this->db->get('palpites')->result();
    }  

    public function findByEmail($email) {
        $this->db->where('email', $email);
        return $this->db->get('palpites')->result();
    }     
}