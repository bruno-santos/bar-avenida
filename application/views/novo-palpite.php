<?php if ($flashdata) { ?>
  <div class="callout alert">
    <h5>Email Duplicado</h5>
    <p><?= $flashdata ?></p>
    <a href="<?= base_url() ?>palpites/novo">Votar com outro email.</a>
  </div>
<?php } ?>

<div class="row title">
  <div class="medium-12 columns">
    <h1>Novo Palpite</h1>
  </div>
</div>
<div class="row">
  <div class="medium-12 columns">
    <form action="<?= base_url() ?>palpites/incluir" method="post" class="form">
      <div class="row">
        <div class="medium-6 columns">
          <label for="">
            Nome:
            <input type="text" required name="nome">
          </label>
        </div>
        <div class="medium-6 columns">
          <label for="">
            Email:
            <input type="text" required name="email">
          </label>
        </div>
      </div>

      <div class="row">
        <div class="medium-6 columns">
          <label for="">
            Gols do Brasil:
            <input type="text" required name="brasil">
          </label>
        </div>
        <div class="medium-6 columns">
          <label for="">
            Gols do Grêmio:
            <input type="text" required name="gremio">
          </label>
        </div>
      </div>

      <div class="row">
        <div class="medium-2 medium-offset-10 columns">
          <button type="submit" class="expanded button">Enviar</button>
        </div>
      </div>
    </form>
  </div>
</div>