<div class="row title">
	<div class="medium-12 columns">
		<h1>Bar Avenida - Gauchão 2015</h1>
		<h4>Acerte o placar do Jogo Brasil(Pel) x Grêmio e concorra a prêmios</h4>
	</div>
</div>

<div class="row home-main-content">
	<div class="medium-6 columns">
		<img src="<?= base_url() ?>public/img/brasilxgremio.jpg" alt="">
	</div>
	<div class="medium-6 columns">
		<p><strong>Data do Jogo: </strong> 07/02/2016</p>
		<p><strong>Horário: </strong> 17h</p>
		<p><strong>Local: </strong> Estádio Bento Freitas</p>
		<p><strong>Prêmios: </strong> Camisetas oficiais e bolas de futebol</p>
		<p><strong>Obs: </strong> Um palpite por cliente</p>
		<p><strong>Regulamento: </strong> Veja os detalhes da promoção <a href="javascript:;">clicando aqui</a></p>
	</div>
</div>

<div class="row">
	<div class="medium-12 columns text-center">
		<a href="palpites/novo" class="button">Incluir Palpite</a>
		<a href="palpites" class="button">Listar Palpites</a>
		<a href="webservice" class="button">Webservice</a>
	</div>
</div>