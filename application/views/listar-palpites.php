<div class="row title">
  <div class="medium-12 columns">
    <h1>Todos os palpites</h1>
  </div>
</div>

<div class="row">
  <div class="medium-12 columns">
    <table class="table">
      <thead>
        <tr>
          <th width="240">Nome</th>
          <th width="240">Email</th>
          <th width="240">Gols do Brasil</th>
          <th width="240">Gols do Grêmio</th>
          <th width="240">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          foreach ($palpites as $palpite) {
        ?>
            <tr>
              <td><?= $palpite->nome ?></td>
              <td><?= $palpite->email ?></td>
              <td><?= $palpite->brasil ?></td>
              <td><?= $palpite->gremio ?></td>
              <td><a href="<?= base_url() ?>palpites/excluir/<?= $palpite->id ?>" class="expand alert button">Excluir</a></td>
            </tr>
          
        <?php 
          }
        ?>
    </table>
  </div>
</div>

<div class="row">
  <div class="medium-12 columns">
    <a href="<?= base_url() ?>" class="button">Voltar para a Home</a>
  </div>
</div>