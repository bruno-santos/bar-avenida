<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */

  public function __construct() {
    parent::__construct();
    $this->load->model('palpitesModel', 'palpites');
  }

  public function index() {
    // indica o tipo de retorno do método
    header("Content-type:application/xml");

    // inicializa a biblioteca SimpleXML
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><palpites></palpites>');

    // obtém o registro do id passado
    $dados = $this->palpites->select();

    // verifica se encontrou algum registro
    for ($i=0; $i < count($dados); $i++) {
        $palpite = $xml->addChild('palpite');
        $palpite->addChild('nome', $dados[$i]->nome);
        $palpite->addChild('email', $dados[$i]->email);
        $palpite->addChild('brasil', $dados[$i]->brasil);
        $palpite->addChild('gremio', $dados[$i]->gremio);
    }

    // mostra no formato xml
    echo $xml->asXML();
  }

  public function find($id) {
    
    // indica o tipo de retorno do método
    header("Content-type:application/xml");

    // inicializa a biblioteca SimpleXML
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><palpites></palpites>');

    // verifica se $id não foi passado
    if ($id == null) {
        
    } else {
        // obtém o registro do id passado
        $dados = $this->palpites->find($id);

        // verifica se encontrou algum registro
        if (count($dados) == 1) {
            $palpite = $xml->addChild('palpite');
            $palpite->addChild('nome', $dados[$id]->nome);
            $palpite->addChild('email', $dados[$id]->email);
            $palpite->addChild('brasil', $dados[$id]->brasil);
            $palpite->addChild('gremio', $dados[$id]->gremio);
        } else {
            $palpite = $xml->addChild('palpite');
            $palpite->addChild('nome', 'inexistente');
            $palpite->addChild('email', null);
            $palpite->addChild('brasil', null);
            $palpite->addChild('gremio', null);
        }
    }

    // mostra no formato xml
    echo $xml->asXML();
  }
}
