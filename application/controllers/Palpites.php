<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Palpites extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		$this->load->model('palpitesModel', 'palpites');
	}

	public function index()
	{
		$data['palpites'] = $this->palpites->select();

		$this->load->view('inc/header');
		$this->load->view('listar-palpites', $data);
		$this->load->view('inc/footer');
	}

	public function novo()
	{
		$data['flashdata'] = $this->session->flashdata('message');

		$this->load->view('inc/header');
		$this->load->view('novo-palpite', $data);
		$this->load->view('inc/footer');
	}

	public function incluir() {
		$data = $this->input->post();

		$duplicatedEmail = $this->palpites->findByEmail($data['email']);

		if (empty($duplicatedEmail)) {
			$inc = $this->palpites->insert($data);

			($inc) ? redirect("/palpites") : var_dump("error");
		} else {
			$this->session->set_flashdata('message', 'Já existe um voto computado com este email.');
			redirect("/palpites/novo");
		}

	}

	public function excluir($id) {
		$del = $this->palpites->delete($id);

		($del) ? redirect("/palpites") : var_dump("error");
	}
}
